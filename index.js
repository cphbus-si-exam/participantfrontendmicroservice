const express = require('express');
var bodyParser = require('body-parser')
const fetch = require('node-fetch');

const config = require('./config');

const app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
const port = 3000;



(async() => {

    app.get("/event/:eventid", async (req, res)=>{
        console.log(config.participant_system+"/event/"+req.params.eventid+"/participant")
        console.log(config.eventms+"/event/"+req.params.eventid)
        let participantP = fetch(config.participant_system+"/event/"+req.params.eventid+"/participant").then(r => r.json());
        let eventInfoP = fetch(config.eventms+"/event/"+req.params.eventid).then(r => r.json());
        let [participants, event] = await Promise.all([participantP, eventInfoP]);
        res.json({participants: participants.participants, event})
    })


    // Setting up the public directory
    app.use(express.static('static'));

    app.listen(port, () => console.log(`listening on port ${port}!`));


})()
